## **TEST PLAN** ## 

## **"The Art Of Healthy Living" - Social Network** ##

## Prepared by:
- *Mariana Sheytanova*
- *Savina Marinova*
- *Petar Petrunov*

### Date: 24.10.2020
--------
## **TABLE OF CONTENTS** ##
1. **INTRODUCTION**
2. **OBJECTIVES AND TASKS**
* OBJECTIVES
* TASKS
3. **SCOPE**
* FUNCTIONALITY TO BE TESTED
* FUNCTIONALITY NOT TO BE TESTED
4. **APPROACH**
5. **EXIT CRITERIA**
6. **SCHEDULE AND TASK ESTIMATION**
7. **TOOLS**
8. **ENVIRONMENT REQUIREMENTS**

-----
1. ### **INTRODUCTION**

The product that shall be tested is a Social network web application, called The Art Of Healthy Living. The application enables users to:
* Connect with people
* Create, comment and like posts
* Get a feed of the newest/most relevant posts of their connections
<br>

The application contains:
* Public part that is visible without authentication
* Private part for registered users accessible after successful login
* Administration Part - system administrators should have administrative access to the system and permissions to administer all major information objects in the system


2. ### **OBJECTIVES AND TASKS** ##
 ### OBJECTIVES 
This test plan has been created to facilitate communication within the project team. <br>
Based on the above mentioned functionalities, we define the objective of the project <The Art Of Healthy Living> as follow:
* To verify that all reqired functionalities work properly
* To make sure that the application is user-friendly
* To identify the deffects in the apllication
* To assure that the quality of the product is at the required level
 ### TASKS ###
The tasks that this plan identifies are:
* To define which functionalities of the application will be tested
* To define the functionalities which will not be tested
* To define the test types and techniques that will be used during the testing process
* To describe task estimation and schedule of the project team
* To define the exit criteria of the testing process

3. ### **SCOPE** ###
 ### **Functionalities to be tested** ##
#### Public part visible without authentication 
* Sign up
* Public feed
* Public profile
#### Private part - after successful login
* Log in/Log out 
* Profile update
* Search and connect with people
* View posts. Check chronological order
* Post creation
* Comment/Like posts
#### Administration part
* Edit/delete profiles
* Edit/delete posts
* Edit/delete comments

### **Functionalities not to be tested**
The following features will not be tested because they are not with high priority and are optional for the project:
* Comment reply functionality
* Upload a song and location in the posts
* Change email from profile administration page

4. ### **APPROACH** ###
High level test cases will be developed in order to verify the required quality level.
The below mentioned testing types will be covered by the project team in order to be performed different user scenarios. 
* Exploratory testing will be executed covering the Public part.
The Exploratory testing session will be between 30 and 60 minutes. The testing process will be documented and the occurred issues will be reported. 
* Manual and automation testing of the main functionalities of the Public, Private and Administration parts will be done. For recording of the test cases we will use one common Test Case Template, attached in GitLab/Final Project - Tir Na Nog Team Repository/"Templates" folder. <br>All test cases will be stored in TestRail management tool. <br> The issues will be logged in GitLab/Final Project - Tir Na Nog Team Repository.
- The automation testing will be done with Selenium Web Driver. Some of the tests will be covered using BDD.
- Web service tests will be created using Postman.

5. ### **EXIT CRITERIA**
* All functionalities in Scope were tested
* All test cases are executed
* All issues were documented and reported
* Completed report of the overall testing process

6. ### **SCHEDULE AND TASK ESTIMATION** ###


|   Activity|  Responsibility |   Estimation  | Schedule |
|---------  |-----------------|---------|----------------|
|  Analyze of documentation| Mariana, Savina, Petar |  1 day | 24.10.2020|
| Test Plan creation |  Mariana, Savina, Petar |  2 days  |24.102020 - 25.10.2020|
|Set up of testing environment and templates| Mariana, Savina, Petar|1 day|30.10.2020|
|Exploratory Testing - Sign up| Savina| 1 day| 01.11.2020|
|Exploratory Testing - Public Feed|Petar|1 day|01.11.2020|
|Exploratory Testing - Public Profile|Mariana | 1 day| 01.11.2020|
| Creation of test cases - Public part functionalities|Mariana | 5 days|06.11.2020 - 10.11.2020|
|Creation of test cases - Private part functionalities|Savina| 5 days|06.11.2020 - 10.11.2020|
|Creation of test cases - Administration part functionalities|Petar|5 days|06.11.2020 - 10.11.2020|
| Manual testing - Public Part | Mariana | 3 days|11.11.2020 - 13.11.2020|
| Manual testing - Private part | Savina | 3 days|11.11.2020 - 13.11.2020|
| Manual testing - Administration part | Petar | 3 days|11.11.2020 - 13.11.2020|
|Postman Rest API Tests|Mariana, Savina, Petar|4 days|14.11.2020 - 17.11.2020|
| Selenium Web Driver Tests | Mariana, Savina, Petar| 5 days|18.11.2020 - 22.11.2020|
| Reporting and closure activities | Mariana, Savina, Petar| 4 days| 23.11.2020 - 26.11.2020|

<br>

7. ### **TOOLS** ###
* Exploratory Testing tool: XRay
* Test Case management tool: TestRail
* Bug logging/tracking tool: GitLab
* Automation testing: Selenium WebDriver
* BDD testing: JBehave
* Web services testing: Postman
* Source code management tool: GitLab
* JDE: IntelliJ
* Organizing and planning tool: Trello

8. ### **ENVIRONMENT REQUIREMENTS** ###

#### PC 1
* OS: Windows 10 Pro
* Processor: Intel(R) Pentium(R) Silver N5000 CPU @ 1.10 GHz
* RAM: 4,00 GB
* System Type: 64-bit Windows OS

#### PC 2
* OS: Windows 10 Pro 
* Processor: Build 19042.630 CPU: Core i7 920 4Ghz
* RAM: 12 GB DDR3 1600Mhz
* System Type: 64-bit Windows OS

#### PC 3
* OS: Windows 10 Enterprise
* Processor: AMD A6-5200 APU with Radeon(TM) HD Graphics 2.00 GHz
* RAM: 8,00 GB
* System Type: 64-bit Windows OS
#### BROWSERS: 
Google Chrome V 86.0.4240.183 <br>
Microsoft Edge V 86.0.622.69 <br>
Mozilla Firefox V 73



