##  **Final Project - Social Network Testing**

</br>

### We are Mariana, Savina and Petar from **Tir Na Nog Team**. </br>
### Please, find in the list below, links to additional files and resources used in the project. 

### 1. Exploratory Testing - Testing Sessions and Report: https://telerikacademy-my.sharepoint.com/:b:/p/savina_marinova_a22_learn/EfRjGr9MrtZOqShnOeSGGO8B6JkZyZG3g07OM9r9zyMHhA?e=uqWT7h 
### 2. Test Cases managed in TestRail: https://tirnanog.testrail.io/index.php?/projects/overview/1 <br>
### username: tirnanog044@gmail.com </br>
### password: KgLK4UGaxWKYMYoEiAwu
### 3. Tests Run Report: https://telerikacademy-my.sharepoint.com/:b:/p/savina_marinova_a22_learn/EStwE4Cx1alLss4GEYaaSlEB-aQZKV3kI_faehru79DVUg?e=WJ1Bqc
### 4. Final Report: https://telerikacademy-my.sharepoint.com/:b:/p/savina_marinova_a22_learn/ER0gXz_qpGVJlVnqTVQC704B-Ko1Rmj0Q1zRQ4h0u9RhWQ?e=n0cCRA
### 5. Project Presentation: https://www.canva.com/design/DAEPTY3qyb0/BdX8ueVX0TiT3DQFO0qE_Q/edit

## **Instructions for test executions**

### 1. Prerequisites for starting the project:
* Install IntelliJ IDEA Community Edition
* Install JDK 11
* GitLab Project Repository link: https://gitlab.com/savina6464/final-project-tir-na-nog-team
* Environment: Stage 6
* Base url: https://fast-taiga-64065.herokuapp.com/


### 2. Prerequisites for running Postman tests:
* Install Postman 7.36.0
* Install Node.js v12.19.0.
* Install Newman (from CMD -> npm install -g newman)
* Newman reporter (from CMD -> npm install -g newman-reporter-htmlextra)
* Run .bat file in Postman folder

### 3. Prerequisites for running Selenium Tests:
* Install IntelliJ IDEA Community Edition
* Install JDK 11
* Install Maven
* Install Selenium WebDriver
* Install JBehave Pluggins
* Run .bat file in SeleniumWebDriverTests folder

