## **Issue logging template** ##

1. ID:
2. Date:
3. Summary:
4. Steps to reproduce:
5. Actual result:
6. Expected result: 
7. Reporter:
8. Priority:
9. Severity:
10. Environment:


