package com.telerikacademy.finalproject.pages;

import org.junit.Assert;

public class LogOutPage extends BasePage {
    public LogOutPage() {
        super("logOut.url");
    }

    public void logOut() {
        actions.clickElement("logOut.Button");
        actions.waitMillis(1000);
    }
}
