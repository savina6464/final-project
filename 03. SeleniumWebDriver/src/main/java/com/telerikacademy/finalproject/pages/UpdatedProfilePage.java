package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.UserActions;
import com.telerikacademy.finalproject.utils.Utils;

public class UpdatedProfilePage extends BasePage {
    public UpdatedProfilePage() {
        super("profileUpdate.url");
    }
    public void assertLastNameChanged(String name){
        navigateToPage();
        actions.assertTextExists(name);
    }
    public void assertPictureVisibilityChangedToConnections () {
        navigateToPage();
        actions.waitForElementVisible("pictureVisibility.SetToConnections", 10);
        actions.clickElement("saveProfile.Button");
    }

}
