package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.UserActions;
import com.telerikacademy.finalproject.utils.Utils;

public class AdminPostPage extends BasePage {
    public AdminPostPage() {
        super("posts.url");
    }

    public void createPost() {
        actions.waitForElementVisible("navigation.LatestPosts", 30);
        actions.moveToElement("navigation.LatestPosts");
        actions.waitForElementVisible("newPost.Section", 30);
        actions.clickElement("newPost.Section");
        actions.waitForElementVisible("newPost.Title", 30);
        actions.typeValueInField(UserActions.getTextWithKey("post.Title"), "newPost.Title");
        actions.clickElement("savePost.Button");
        actions.waitMillis(1000);
    }

    public void assertPostCreated() {
        actions.moveToElement("navigation.LatestPosts");
        actions.clickElement("navigation.LatestPosts");
        actions.waitForElementVisible("select.Post", 30);
        actions.clickElement("select.Post");
        actions.waitForElementVisible("assertPost.exists", 30);
        actions.assertElementPresent("assertPost.exists");
        actions.waitMillis(1000);
    }

    public void editPost() {
        actions.moveToElement("navigation.LatestPosts");
        actions.clickElement("navigation.LatestPosts");
        actions.waitForElementVisible("select.Post", 30);
        actions.clickElement("select.Post");
        actions.waitForElementVisible("editPost.Button", 30);
        actions.clickElement("editPost.Button");
        actions.clearField("editPost.Title");
        actions.typeValueInField(UserActions.getTextWithKey("updated.Post"), "editPost.Title");
        actions.clickElement("savePost.Button");
    }

    public void assertPostEdited(String updatedPostTitle) {
        actions.waitForElementVisible("navigation.LatestPosts", 30);
        actions.clickElement("navigation.LatestPosts");
        actions.waitForElementVisible("search.Post", 30);
        actions.typeValueInField(UserActions.getTextWithKey(updatedPostTitle), "search.Post");
        actions.clickElement("searchPost.Button");
        actions.waitForElementVisible("select.Post", 30);
        actions.clickElement("searchedPost.Result");
        actions.waitMillis(1000);
    }

    public void deletePost() {
        actions.waitForElementVisible("navigation.LatestPosts", 30);
        actions.clickElement("navigation.LatestPosts");
        actions.waitForElementVisible("searchedPost.Result", 30);
        actions.clickElement("searchedPost.Result");
        actions.waitForElementVisible("editPost.Button", 30);
        actions.clickElement("editPost.Button");
        actions.waitForElementVisible("postDelete.Button", 30);
        actions.clickElement("postDelete.Button");
        actions.waitForElementVisible("confirmDelete.Button", 30);
        actions.clickElement("confirmDelete.Button");

    }
}

