package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.UserActions;

public class LatestPostsPage extends BasePage {
    public LatestPostsPage() {
        super("posts.url");
    }

    public void searchPostByTitle(String title) {
        actions.waitForElementVisible("search.Post", 20);
        actions.typeValueInField(UserActions.getTextWithKey(title), "search.Post");
        actions.clickElement("select.Post");
        actions.waitMillis(1000);

    }

    public void assertPostFound() {
        navigateToPage();
        actions.waitForElementVisible("post.Area", 20);
        actions.assertElementPresent("searchedPost.Result");
        actions.waitMillis(1000);

    }

    public void openPost() {
        actions.waitForElementVisible("select.Post", 20);
        actions.clickElement("select.Post");
        actions.waitMillis(1000);
    }
    public void likePost() {
        openPost();
        actions.waitForElementVisible("like.Post", 20);
        actions.clickElement("like.Post");
        actions.waitMillis(1000);
    }
    public void assertPostLiked () {
        actions.waitForElementVisible("dislike.Button", 20);
        actions.assertElementPresent("dislike.Button");
        actions.clickElement("dislike.Button");
        actions.waitMillis(1000);
    }
    public void commentPost () {
        openPost();
        actions.waitForElementVisible("comment.TextArea", 20);
        actions.typeValueInField(UserActions.getTextWithKey("comment.Post"), "comment.TextArea");
        actions.clickElement("sendComment.Button");
        actions.waitMillis(1000);
    }
    public void assertCommentPosted (String commentText) {
        actions.waitForElementVisible("comment.Area", 20);
        actions.assertTextExists(commentText);
        actions.waitMillis(1000);

    }


}
