package com.telerikacademy.finalproject.pages;

public class UsersPage extends BasePage {
    public UsersPage() {
        super("users.url");
    }

    public void navigationToUsersPage() {
        navigateToPage();
        actions.clickElement("users.HomePageNavigate");
        actions.waitMillis(1000);
    }
    public void searchByEmail(String email) {
        navigateToPage();
        actions.waitForElementVisible("search.Field", 30);
        actions.typeValueInField(email, "search.Field");
        actions.clickElement("search.Button");
        actions.waitMillis(1000);

    }
    public void assertUserFoundByEmail(String email){
        actions.waitForElementVisible("searchedUser.results", 30);
        actions.waitMillis(1000);
        actions.assertTextExists(email);
        actions.waitMillis(1000);
    }
    public void searchUserByName(String name) {
        navigateToPage();
        actions.waitForElementVisible("search.Field", 30);
        actions.typeValueInField(name, "search.Field");
        actions.clickElement("search.Button");
        actions.waitMillis(1000);

    }
    public void assertUserFoundByName (String name) {
        actions.waitForElementVisible("searchedUser.results", 30);
        actions.waitMillis(1000);
        actions.assertTextExists(name);
        actions.waitMillis(1000);
    }

    @Override
    public void assertPageNavigated() {
        super.assertPageNavigated();
    }
}
