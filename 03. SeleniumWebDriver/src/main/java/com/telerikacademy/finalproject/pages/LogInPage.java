package com.telerikacademy.finalproject.pages;

import org.junit.Assert;

public class LogInPage extends BasePage {

    public LogInPage() {
        super("logIn.url");
    }

    public void logIn(String username, String password) {
        actions.waitForElementVisible("name.Field", 30);
        actions.typeValueInField(username, "name.Field");
        actions.moveToElement("password.Field");
        actions.typeValueInField(password, "password.Field");
        actions.moveToElement("logIn.Button");
        actions.clickElement("logIn.Button");
        actions.waitMillis(1000);
    }

    public void assertSuccessfulLogIn() {
        NavigationPage navigationPage = new NavigationPage();
        navigationPage.assertPageNavigated();
    }
    public void assertWrongPasswordMessage(String text) {
        Assert.assertTrue("Wrong username or password.", driver.getPageSource().contains(text));
    }
}
