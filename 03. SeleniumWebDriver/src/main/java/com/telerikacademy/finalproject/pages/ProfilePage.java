package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.UserActions;

public class ProfilePage extends BasePage {
    public ProfilePage() {
        super("profilePage.url");
    }
    public void updateUserLastName () {
        actions.waitForElementVisible("editProfile.Button", 20);
        actions.clickElement("editProfile.Button");
        actions.waitForElementVisible("lastName.Field", 20);
        actions.clearField("lastName.Field");
        actions.typeValueInField(UserActions.getTextWithKey("updated.lastName"),"lastName.Field");
        actions.moveToElement("saveProfile.Button");
        actions.clickElement("saveProfile.Button");
    }
    public void updateProfilePictureVisibility (){
        actions.waitForElementVisible("editProfile.Button", 20);
        actions.clickElement("editProfile.Button");
        actions.waitForElementVisible("pictureVisibility.Field", 20);
        actions.clickElement("pictureVisibility.Field");
        actions.moveToElement("saveProfile.Button");
        actions.clickElement("saveProfile.Button");
    }


}
