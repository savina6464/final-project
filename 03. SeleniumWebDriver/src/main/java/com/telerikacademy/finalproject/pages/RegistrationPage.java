package com.telerikacademy.finalproject.pages;

public class RegistrationPage extends BasePage {

    public RegistrationPage() {
        super("registration.url");
    }
    public void navigateToRegistrationPage(){
        actions.clickElement("navigation.SignUp");
        actions.waitMillis(1000);
    }

    @Override
    public void assertPageNavigated() {
        super.assertPageNavigated();
    }


}
