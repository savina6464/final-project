package com.telerikacademy.finalproject.pages;

public class NavigationPage extends BasePage {
    public NavigationPage() {
        super("base.url");
    }

    public final String homeButton = "navigation.Home";
    public final String logOutButton = "navigation.LogOut";
    public final String signUpButton = "navigation.SignUp";

    public void navigateToPage() {
    }
}