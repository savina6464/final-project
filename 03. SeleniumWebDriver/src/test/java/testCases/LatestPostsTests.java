package testCases;

import com.telerikacademy.finalproject.pages.LatestPostsPage;
import com.telerikacademy.finalproject.pages.LogInPage;
import com.telerikacademy.finalproject.pages.LogOutPage;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class LatestPostsTests extends BaseTest {
    private static LogInPage loginPage = new LogInPage();
    private static final LogOutPage logOutPage = new LogOutPage();
    private static final LatestPostsPage latestPostsPage = new LatestPostsPage();
    private static final String title = "post.Title";
    private static final String username = "savinamarinova5@gmail.com";
    private static final String password = "aaa123AAA@";
    private static final String commentText = "Selenium comment";


    @BeforeClass
    public static void testInit() {
        loginPage.navigateToPage();
        loginPage.logIn(username, password);
        latestPostsPage.navigateToPage();
    }

    @AfterClass
    public static void testsTearDown() {
        logOutPage.logOut();
    }

    @Test
    public void TC118_SearchPostByTitle() {
        latestPostsPage.searchPostByTitle(title);
        latestPostsPage.assertPostFound();
    }

    @Test
    public void TC119_LikePost() {
        latestPostsPage.likePost();
        latestPostsPage.assertPostLiked();
    }

    @Test
    public void TC122_CommentPost() {
        latestPostsPage.commentPost();
        latestPostsPage.assertCommentPosted(commentText);

    }
}