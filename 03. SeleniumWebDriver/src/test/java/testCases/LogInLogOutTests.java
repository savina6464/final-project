package testCases;

import com.telerikacademy.finalproject.pages.LogInPage;
import com.telerikacademy.finalproject.pages.LogOutPage;
import com.telerikacademy.finalproject.pages.NavigationPage;
import org.junit.Before;
import org.junit.Test;

public class LogInLogOutTests extends BaseTest {
    private static final String username = "savinamarinova5@gmail.com";
    private static final String password = "aaa123AAA@";
    private static final String invalidPassword = "aaa123AAA#";
    private static final String errorMessage = "Wrong username or password.";
    private final NavigationPage navigationPage = new NavigationPage();
    private final LogInPage logInPage = new LogInPage();
    private final LogOutPage logOutPage = new LogOutPage();

    @Before
    public void preconditions() {
        logInPage.navigateToPage();
    }

    @Test
    public void TC105_UnsuccessfulLogInWithInvalidPassword() {
        logInPage.logIn(username, invalidPassword);
        logInPage.assertWrongPasswordMessage(errorMessage);

    }

    @Test
    public void TC104_SuccessfulLogInWithValidCredentials() {
        logInPage.logIn(username, password);
        logInPage.assertSuccessfulLogIn();

    }

    @Test
    public void TC107_SuccessfulLogOut() {
        logInPage.logIn(username, password);
        logOutPage.logOut();
        logOutPage.assertPageNavigated();

    }

}