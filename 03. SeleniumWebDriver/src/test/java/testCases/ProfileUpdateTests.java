package testCases;

import com.telerikacademy.finalproject.pages.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class ProfileUpdateTests extends BaseTest {
    private static final String username = "savinamarinova5@gmail.com";
    private static final String password = "aaa123AAA@";
    private static final String updatedLastName = "Marinova";
    private static final LogInPage loginPage = new LogInPage();
    private static final ProfilePage profilePage = new ProfilePage();
    private static final LogOutPage logOutPage = new LogOutPage();
    private static final UpdatedProfilePage updatedProfilePage = new UpdatedProfilePage();


    @BeforeClass
    public static void testInit() {
        loginPage.navigateToPage();
        loginPage.logIn(username, password);
        profilePage.navigateToPage();
    }

    @AfterClass
    public static void testsTearDown() {
        logOutPage.logOut();
    }

    @Test
    public void TC108_UpdateUserByLastName() {
        profilePage.updateUserLastName();
        updatedProfilePage.assertLastNameChanged(updatedLastName);
    }

    @Test
    public void TC110_ChangeProfilePictureVisibility() {
        profilePage.updateProfilePictureVisibility();
        updatedProfilePage.assertPictureVisibilityChangedToConnections();

    }
}
