package testCases;

import com.telerikacademy.finalproject.pages.LatestPostsPage;
import com.telerikacademy.finalproject.pages.NavigationPage;
import com.telerikacademy.finalproject.pages.RegistrationPage;
import com.telerikacademy.finalproject.pages.UsersPage;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class PublicPartTests extends BaseTest {
    private static final String email = "savinamarinova5@gmail.com";
    private static final String name = "Savina";
    NavigationPage navigationPage = new NavigationPage();
    RegistrationPage registrationPage = new RegistrationPage();
    LatestPostsPage latestPostsPage = new LatestPostsPage();
    UsersPage usersPage = new UsersPage();


    @Before
    public void navigateToHomePage() {
        navigationPage.navigateToPage();
    }

    @AfterClass
    public static void quitDriver() {

    }

    @Test
    public void TC136_NavigateToRegistrationForm_UsingNavigation() {
        registrationPage.navigateToRegistrationPage();
        registrationPage.assertPageNavigated();
    }

    @Test
    public void TC138_NavigateToLatestPostPage_UsingNavigation() {
        latestPostsPage.navigateToPage();
        latestPostsPage.assertPageNavigated();
    }

    @Test
    public void TC_140_NavigateToUsersPage_UsingNavigation() {
        usersPage.navigationToUsersPage();
        usersPage.assertPageNavigated();

    }

    @Test
    public void TC139_SearchUserByEmail() {
        usersPage.searchByEmail(email);
        usersPage.assertUserFoundByEmail(email);

    }

    @Test
    public void TC141_SearchUserByName() {
        usersPage.searchUserByName(name);
        usersPage.assertUserFoundByName(name);
    }

}
