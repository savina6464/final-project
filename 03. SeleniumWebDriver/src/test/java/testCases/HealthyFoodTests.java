package testCases;

import com.telerikacademy.finalproject.pages.NavigationPage;
import org.junit.Test;

public class HealthyFoodTests extends BaseTest {

    @Test
    public void navigateToHome_UsingNavigation() {

        tastyFoodAPI.authenticateDriverForUser("healthyFoodAdmin.username.encoded", "healthyFoodAdmin.pass.encoded", actions.getDriver());

        NavigationPage navPage = new NavigationPage();
        actions.clickElement(navPage.homeButton);
        navPage.assertPageNavigated();
        actions.assertElementPresent(navPage.logOutButton);
    }
}
