package testCases;

import com.telerikacademy.finalproject.pages.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class AdminPostsTests extends BaseTest {
    private static final String title = "post.Title";
    private static final String updatedPostTitle = "updated.Post";
    private static final String username = "stage.6.final.project@gmail.com";
    private static final String password = "Admin123$";
    private static final LogInPage loginPage = new LogInPage();
    private static final LogOutPage logOutPage = new LogOutPage();
    private static final AdminPostPage adminPostPage = new AdminPostPage();
    private static final UsersPage usersPage = new UsersPage();
    private static final LatestPostsPage latestPostPage = new LatestPostsPage();

    @BeforeClass
    public static void testInit() {
        loginPage.navigateToPage();
        loginPage.logIn(username, password);

    }

    @AfterClass
    public static void testsTearDown() {
        logOutPage.logOut();
    }

    @Test
    public void TC115_CreateNewPost() {
        adminPostPage.createPost();
        adminPostPage.assertPostCreated();
    }

    @Test
    public void TC185_EditPost() {
        adminPostPage.editPost();
        adminPostPage.assertPostEdited(updatedPostTitle);

    }

    @Test
    public void TC_191_DeletePost() {
        adminPostPage.deletePost();
        usersPage.assertPageNavigated();


    }
}
